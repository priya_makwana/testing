{
    'name': 'Easy to Import sale orders',
    'category': 'sales',
    'version': '1.0',
    'description':
        """
        Import sale orders
        """,
    'depends': ['sale_management'],
    'data': [
        'views/easy_import_sale_orders.xml',
        'views/import_process_log.xml',
        'views/res_config_settings_views.xml',
        'views/sale_order.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True,
}
