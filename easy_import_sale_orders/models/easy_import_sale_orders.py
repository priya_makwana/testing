import csv
import base64
from odoo import api, fields, models
from odoo.exceptions import Warning
from io import StringIO

class EasyImportSaleOrders(models.TransientModel):
    _name = 'easy.import.sale.orders'
    _rec_name = 'import_file'
    import_file = fields.Binary("Load File")
    
    def import_sale_order(self):
        if not self.env.user.company_id.shipping_product_id:
            raise Warning('You have need configure shipping product.')
        
        csv_file = StringIO(base64.decodestring(self.import_file).decode())
        file_write = open('/tmp/import_sale_order.csv', 'w+')
        file_write.writelines(csv_file.getvalue())
        file_write.close()
        
        with open('/tmp/import_sale_order.csv', 'r') as csvfile:
                csv_reader = csv.DictReader(csvfile)
                log_obj = self.env['import.process.log'].create({'file_name': 'import_sale_order.csv'})
                for row in csv_reader:
                    try : 
                        product_obj = False
                        customer_obj = False
                        currency_obj = self.env['res.currency'].search([('name','=ilike',row['Currency']),'|',('active','=',False),('active','=',True)])
                        if not currency_obj: 
                            currency_obj = self.env['res.currency'].create({'name': row['Currency'],'symbol':row['Currency'],'active':True})
                            self.env['import.process.log.line'].create({'name' :'Currency created id is %s'%(currency_obj.id),'import_process_log_id' : log_obj.id })
                        else : 
                            if not currency_obj.active : 
                                currency_obj.write({'active' : True})
                                self.env['import.process.log.line'].create({'name' :'Currency Activated id is %s'%(currency_obj.id),'import_process_log_id' : log_obj.id })
                                
                        product_pricelist_obj = self.env['product.pricelist'].search([('country_group_ids','=',False),('currency_id','=',currency_obj.id)],limit=1)
                        if not product_pricelist_obj :
                            product_pricelist_obj = self.env['product.pricelist'].create({'name':'Public Pricelist','currency_id' : currency_obj.id})
                            self.env['import.process.log.line'].create({'name' :'Product Pricelist Created id is %s'%(product_pricelist_obj.id),'import_process_log_id' : log_obj.id })
                            
                        if row['Customer Code']:
                            customer_obj = self.env['res.partner'].search([('ref','=',row['Customer Code'])],limit=1)
                            if customer_obj and customer_obj.property_product_pricelist:
                                if customer_obj.property_product_pricelist.currency_id.name != row['Currency']:
                                    self.env['import.process.log.line'].create({'name' :'Customer currency mismatch customer id is: %s and Customer code is %s'%(customer_obj.id,customer_obj.ref),'import_process_log_id' : log_obj.id })
                            if not customer_obj:
                                customer_obj = self.env['res.partner'].create({'name':row['Name'],'email': row['Email'],'ref' : row['Customer Code'], 'property_product_pricelist' : product_pricelist_obj.id})
                                self.env['import.process.log.line'].create({'name' :'Partner created id is %s'%(customer_obj.id),'import_process_log_id' : log_obj.id})
                        
                        if row['SKU']:
                            product_obj = self.env['product.product'].search([('default_code','=',row['SKU'])])
                            if not product_obj:
                                product_obj = self.env['product.product'].create({'name':row['Product Description'],'default_code':row['SKU'],'standard_price': row['Cost Per Unit']})
                                self.env['import.process.log.line'].create({'name' :'Product created id is %s'%(product_obj.id),'import_process_log_id' : log_obj.id})
                                product_price_obj = self.env['product.pricelist.item'].create({'pricelist_id':product_pricelist_obj.id,'fixed_price':row['Price'],'product_tmpl_id':product_obj.product_tmpl_id.id})
                                self.env['import.process.log.line'].create({'name' :'Product pricelist item created id is %s'%(product_price_obj.id),'import_process_log_id' : log_obj.id})
                            else:
                                filter_pricing_obj = product_obj.item_ids.filtered(lambda x: product_pricelist_obj.id == x.pricelist_id.id)
                                flag = False
                                for filter_pricing in filter_pricing_obj :
                                    if float(row['Price']) == filter_pricing.fixed_price:
                                        flag = True
                                        break
                                if flag == False:
                                    product_price_obj = self.env['product.pricelist.item'].create({'pricelist_id':product_pricelist_obj.id,'fixed_price':row['Price'],'product_tmpl_id':product_obj.product_tmpl_id.id})
                                    self.env['import.process.log.line'].create({'name' :'Product pricelist item created id is %s'%(product_price_obj.id),'import_process_log_id' : log_obj.id})
                                    
                        if customer_obj :
                            sale_order_obj = self.env['sale.order'].search([('client_order_ref','=',row['Order No']),('partner_id','=',customer_obj.id)])
                            if not sale_order_obj:     
                                sale_order_obj = self.env['sale.order'].create({'date_order':row['Order Date'],'client_order_ref':row['Order No'], 'partner_id': customer_obj.id})
                                vals = {'name' :'Sale order created id is %s'%(sale_order_obj.id),'import_process_log_id' : log_obj.id}
                                if row['Quantity'] and int(row['Quantity']) > 0:
                                    if product_obj:          
                                        order_line_obj = self.env['sale.order.line'].create({'order_id':sale_order_obj.id, 'product_id':product_obj.id, 'product_uom_qty':row['Quantity'], 'price_unit' : row['Price']})
                                        vals.update({'name' : 'Sale order created id is %s, Sale order line Created id is %s'%(sale_order_obj.id, order_line_obj.id)})
                                        if row['Shipping Paid to carrier'] and float(row['Shipping Paid to carrier']) > 0.0:  
                                            shiping_order_line_obj = self.env['sale.order.line'].with_context({'actual_shipping_product' : product_obj}).create({'order_id':sale_order_obj.id, 'product_id':self.env.user.company_id.shipping_product_id.id, 'price_unit' : row['Shipping Paid to carrier']})
                                            vals.update({'name' : 'Sale order created id is %s, Sale order line Created id is %s, Shipping order line created id is %s'%(sale_order_obj.id, order_line_obj.id, shiping_order_line_obj.id)})
                                    else :
                                        self.env['import.process.log.line'].create({'name' :'Sale order line creation is skip due to Product SKU is not found in CSV and data is %s'%(row), 'import_process_log_id' : log_obj.id})
                                self.env['import.process.log.line'].create(vals)
                            else:
                                if product_obj and row['Quantity'] and int(row['Quantity']) > 0:
                                    product_line_obj = sale_order_obj.order_line.filtered(lambda x: product_obj.id == x.product_id.id)
                                    if not product_line_obj:
                                        order_line_obj = self.env['sale.order.line'].create({'order_id':sale_order_obj.id, 'product_id':product_obj.id, 'product_uom_qty':row['Quantity'], 'price_unit' : row['Price']})
                                        if row['Shipping Paid to carrier'] and float(row['Shipping Paid to carrier']) > 0.0:
                                            product_line_obj = sale_order_obj.order_line.filtered(lambda x: self.env.user.company_id.shipping_product_id.id == x.product_id.id)
                                            if not product_line_obj:  
                                                shiping_order_line_obj = self.env['sale.order.line'].with_context({'actual_shipping_product' : product_obj}).create({'order_id':sale_order_obj.id, 'product_id':self.env.user.company_id.shipping_product_id.id, 'price_unit' : row['Shipping Paid to carrier']})
                                    else:
                                        product_line_obj.write({'product_uom_qty':(product_line_obj.product_uom_qty + float(row['Quantity']))})     
                        else:
                            self.env['import.process.log.line'].create({'name' :'Sale order creation is skip due to Customer code is not found in CSV and data is %s'%(row), 'import_process_log_id' : log_obj.id})
                    except Exception as e:
                        self.env['import.process.log.line'].create({'name' :'Exception is %s and CSV data is %s'%(e, row),'import_process_log_id' : log_obj.id})
                        continue  