from odoo import api, fields, models

class ImportProcessLog(models.Model):
    _name = 'import.process.log'
    _description = 'Import Process Log'
    _rec_name = 'file_name'
    
    file_name = fields.Char('File Name')
    log_line_ids = fields.One2many('import.process.log.line', 'import_process_log_id', String='Log Line')