from odoo import fields, models

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    shipping_product_id = fields.Many2one(related='company_id.shipping_product_id', string= "Shipping Product", readonly=False)
