from odoo import api, fields, models

class ImportProcessLogLine(models.Model):
    _name = 'import.process.log.line'
    _description = 'Import Process Log Line'
    
    name = fields.Char(string='Operation', required="1")
    import_process_log_id = fields.Many2one('import.process.log', string ='Log')
    