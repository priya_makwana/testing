from odoo import api, fields, models

class ProductProduct(models.Model):
    _inherit = 'product.product'
    
    @api.multi
    def name_get(self):
        # Shipping product name with Actual product SKU for sale order import only          
        if self._context.get('actual_shipping_product',False) and self._context.get('actual_shipping_product').default_code:
            result = []
            for product in self:
                name = '[' + self._context.get('actual_shipping_product').default_code + ']' + ' ' + self.name
                result.append((product.id, name))
            return result
        return super(ProductProduct, self).name_get()