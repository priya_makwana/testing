from odoo import api, fields, models

class SaleOder(models.Model):
    _inherit = 'sale.order'
    profit = fields.Float(string="Margin", compute='_calculate_profit')
    
    def _calculate_profit(self):
        # Calculate profit for sale order level based on cost per unit         
        for record in self:
            profit = 0
            for order_line in record.order_line:
                if order_line.product_id.id != self.env.user.company_id.shipping_product_id.id :
                    profit = profit + (((order_line.price_unit - order_line.cost_per_unit))*order_line.product_uom_qty)      
            record.profit = profit
    
    