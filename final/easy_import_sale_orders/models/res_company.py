from odoo import fields, models

class ResConfigSettings(models.Model):
    _inherit = 'res.company'
    shipping_product_id = fields.Many2one("product.product", "Shipping Product")
    
    