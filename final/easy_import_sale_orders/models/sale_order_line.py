from odoo import api, fields, models

class SaleOderLine(models.Model):
    _inherit = 'sale.order.line'
    
    cost_per_unit = fields.Float(string="Cost Per Unit")