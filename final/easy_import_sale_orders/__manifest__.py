{
    'name': 'Import Sale Orders',
    'category': 'sales',
    'version': '1.0',
    'description':
        """
        Import Sales Order file in user friendly way 
        """,
    'depends': ['sale_management'],
    'data': [
        'wizard/easy_import_sale_orders.xml',
        'views/import_process_log.xml',
        'views/res_config_settings_views.xml',
        'views/sale_order.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True,
}
