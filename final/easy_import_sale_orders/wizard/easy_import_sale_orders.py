import csv
import base64
from odoo import api, fields, models
from odoo.exceptions import Warning
from io import StringIO

class EasyImportSaleOrders(models.TransientModel):
    _name = 'easy.import.sale.orders'
    _rec_name = 'import_file'
    import_file = fields.Binary("Load File")
    
    def import_sale_order(self):
        if not self.env.user.company_id.shipping_product_id:
            raise Warning('You have need configure shipping product under sales configuration.')
        
        csv_file = StringIO(base64.decodestring(self.import_file).decode())
        file_write = open('/tmp/import_sale_order.csv', 'w+')
        file_write.writelines(csv_file.getvalue())
        file_write.close()
        
        with open('/tmp/import_sale_order.csv', 'r') as csvfile:
                csv_reader = csv.DictReader(csvfile)
                log_obj = self.env['import.process.log'].create({'file_name':'import_sale_order.csv'})
                for row in csv_reader:
                    try: 
                        product_obj = False
                        customer_obj = False
                        # Create and Active currency                         
                        currency_obj = self.env['res.currency'].search([('name', '=ilike', row['Currency']),'|',('active','=',False),('active','=',True)])
                        if not currency_obj: 
                            currency_obj = self.env['res.currency'].create({'name':row['Currency'], 'symbol':row['Currency'], 'active':True})
                            self.env['import.process.log.line'].create({'name':'Currency created (Name:%s and Id:%s)'%(row['Currency'],currency_obj.id),'import_process_log_id':log_obj.id })
                        else: 
                            if not currency_obj.active: 
                                currency_obj.write({'active':True})
                                self.env['import.process.log.line'].create({'name':'Currency Activated (Name:%s and Id:%s)'%(row['Currency'],currency_obj.id),'import_process_log_id':log_obj.id })
                        
                        # Create public price list without any group                                 
                        product_pricelist_obj = self.env['product.pricelist'].search([('country_group_ids','=',False),('currency_id','=',currency_obj.id)],limit=1)
                        if not product_pricelist_obj:
                            product_pricelist_obj = self.env['product.pricelist'].create({'name':'Public Pricelist','currency_id':currency_obj.id})
                            self.env['import.process.log.line'].create({'name':'Product Pricelist Created (Id:%s)'%(product_pricelist_obj.id),'import_process_log_id':log_obj.id })
                        
                        # Create Customer data                            
                        if row['Customer Code']:
                            customer_obj = self.env['res.partner'].search([('ref','=',row['Customer Code'])],limit=1)
                            if customer_obj and customer_obj.property_product_pricelist:
                                if customer_obj.property_product_pricelist.currency_id.name != row['Currency']:
                                    self.env['import.process.log.line'].create({'name':'Customer currency mismatch (Code:%s and Id:%s)'%(customer_obj.ref, customer_obj.id),'import_process_log_id':log_obj.id })
                            if not customer_obj:
                                customer_obj = self.env['res.partner'].create({'name':row['Name'],'email':row['Email'],'ref':row['Customer Code'], 'property_product_pricelist':product_pricelist_obj.id})
                                self.env['import.process.log.line'].create({'name':'Customer created (Code:%s and Id:%s)'%(customer_obj.ref, customer_obj.id),'import_process_log_id':log_obj.id})
                        
                        # Create Product Data                         
                        if row['SKU']:
                            product_obj = self.env['product.product'].search([('default_code','=',row['SKU'])])
                            if not product_obj:
                                product_obj = self.env['product.product'].create({'name':row['Product Description'],'default_code':row['SKU']})
                                self.env['import.process.log.line'].create({'name':'Product created (SKU:%s and Id:%s)'%(product_obj.default_code, product_obj.id),'import_process_log_id':log_obj.id})
                                    
                        # Create sale order, order line and update sale order line                                     
                        if customer_obj:
                            sale_order_obj = self.env['sale.order'].search([('client_order_ref','=',row['Order No']),('partner_id','=',customer_obj.id)])
                            if not sale_order_obj:     
                                sale_order_obj = self.env['sale.order'].create({'date_order':row['Order Date'],'client_order_ref':row['Order No'], 'partner_id': customer_obj.id})
                                self.env['import.process.log.line'].create({'name':'Sale order created (Order No:%s and Id:%s)'%(sale_order_obj.client_order_ref, sale_order_obj.id),'import_process_log_id':log_obj.id})
                            if row['Quantity'] and int(row['Quantity']) > 0:
                                if product_obj:
                                    product_line_obj = sale_order_obj.order_line.filtered(lambda x: product_obj.id == x.product_id.id)
                                    if not product_line_obj:          
                                        order_line_obj = self.env['sale.order.line'].create({'order_id':sale_order_obj.id, 'product_id':product_obj.id, 'product_uom_qty':row['Quantity'], 'price_unit':row['Price'], 'cost_per_unit':row['Cost Per Unit']})
                                        self.env['import.process.log.line'].create({'name':'Sale order line Created (Product:%s and Order No:%s)'%(order_line_obj.product_id.name, sale_order_obj.client_order_ref),'import_process_log_id':log_obj.id})
                                        if row['Shipping Paid to carrier'] and float(row['Shipping Paid to carrier']) > 0.0:
                                            # Create Shipping order line                                            
                                            shipping_product_line_obj = sale_order_obj.order_line.filtered(lambda x: self.env.user.company_id.shipping_product_id.id == x.product_id.id)
                                            if not shipping_product_line_obj:  
                                                shiping_order_line_obj = self.env['sale.order.line'].with_context({'actual_shipping_product':product_obj}).create({'order_id':sale_order_obj.id, 'product_id':self.env.user.company_id.shipping_product_id.id, 'price_unit':row['Shipping Paid to carrier'],'cost_per_unit':row['Shipping Charge']})
                                                self.env['import.process.log.line'].create({'name':'Shipping order line created (Product:%s and Order No:%s)'%(shiping_order_line_obj.product_id.name, sale_order_obj.client_order_ref),'import_process_log_id':log_obj.id})
                                    else:
                                        # update Qty of product with sale order                                         
                                        product_line_obj.write({'product_uom_qty':(product_line_obj.product_uom_qty + float(row['Quantity']))})
                                        self.env['import.process.log.line'].create({'name':'Sale order line QTY updated (Product:%s and Order No:%s)'%(product_line_obj.product_id.name,sale_order_obj.client_order_ref),'import_process_log_id':log_obj.id})
                                else:
                                    self.env['import.process.log.line'].create({'name':'Sale order line creation is skip due to Product SKU is not found in CSV and data is %s'%(row), 'import_process_log_id':log_obj.id})
                        else:
                            self.env['import.process.log.line'].create({'name':'Sale order creation is skip due to Customer code is not found in CSV and data is %s'%(row), 'import_process_log_id':log_obj.id})
                    except Exception as e:
                        self.env['import.process.log.line'].create({'name':'Exception is %s and CSV data is %s'%(e, row),'import_process_log_id':log_obj.id})
                        continue  
                    
                    